<?php

require_once("Db.php");

$db = new Db();
$result = $db->selectData();
$numrows = mysqli_num_rows($result);

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
		<style type="text/css">
			body {
				background: #FFFFF;
			}

			.btn-save {
				/*display: none;*/
			}
		</style>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<table class="table table-hover" id="cust-table">
						<thead>
							<tr>
								<th>LastName</th>
								<th>FirstName</th>
								<th>PhoneNumber</th>
								<th>Email</th>
								<th>Action</th>
								<th class = "add_delete">
									<button class="btn btn-add"><i class="glyphicon glyphicon-plus"></i></button>
									<button class="btn btn-del"><i class="glyphicon glyphicon-minus"></i></button>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php
			                	for($i=0; $i<$numrows; ++$i) {
				                     $contacts = mysqli_fetch_assoc($result);
				                ?>
							<tr>
								<td><?php echo $contacts['LastName']; ?></td>
								<td><?php echo $contacts['FirstName']; ?></td>
								<td><?php echo $contacts['ContactNumber']; ?></td>
								<td><?php echo $contacts['Email']; ?></td>
								<td class="actions">
									<button class="btn btn-danger btn-edit"><i class="glyphicon glyphicon-pencil"></i></button>
									<button class="btn btn-success btn-save"><i class="glyphicon glyphicon-ok"></i></button>
									<input type="hidden" id="SlNo" value="<?php echo $contacts['No']; ?>">
								</td>
							</tr>
							<?php
							} ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="table.js"></script>
	</body>
</html>