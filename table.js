$(function() {
	//$('#cust-table').show(1000);

	$('.btn-edit').click(handleTableEdit);
	$('.btn-save').hide();
	$('.btn-save').click(handleSave);
	$('.btn-add').click(handleAdd);
});

function handleTableEdit(e) {
	var row = $(this).parent().first().parent().first()[0];
	$('.btn-edit').hide();
	$('.btn-save').show();

	$(row).children().each(function () {
		if(!$(this).hasClass('actions')) {
			var value = this.innerText;

			var form = $('<input/>', {
				'class': 'form-control',
				'value': value
			});

			$(this).html(form)
		}
	});
}

function handleSave (e) {
	var row = $(this).parent().first().parent().first()[0]; // console.log(row);
	$('.btn-save').hide();
	$('.btn-edit').show();
	var id = $(this).siblings("#SlNo").val();
	var last_name = $($(row).children().children()[0]).val(); 
	var first_name = $($(row).children().children()[1]).val(); 
	var contact_no = $($(row).children().children()[2]).val(); 
	var email = $($(row).children().children()[3]).val(); 
	//console.log($($(row).children().children()[0]).val());
	//var temp = {LastName:last_name, FirstName:"first_name", ContactNumber:"contact_no", Email:"email", No:"id"};
	//console.log(temp);
	$.ajax({   
       type: 'POST',   
       url: 'update.php',   
       data: {LastName:last_name, FirstName:first_name, ContactNumber:contact_no, Email:email, No:id},
       
    });
	$(row).children().each(function () {
		var td_first_child = $(this).children()[0]; //console.log(td_first_child);

		if(td_first_child != null && $(td_first_child).hasClass('form-control')) {
			var value = $(td_first_child).val();

			$(this).html(value);
		}
	});

}

function handleAdd (e) {
	var row = $(this).parent().parent().parent().parent();
	console.log(row);
}