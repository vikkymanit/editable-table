<?php

/**
*
*/
class Db
{
	private $userName = 'root';
	private $pass = '';
	private $db = 'contacts';
	private $conn;

	function __construct()
	{
		$this->conn = new mysqli('localhost', $this->userName, $this->pass, $this->db) or die("Failed to connect to database");
	}

	public function execQuery($query) {
		return mysqli_query($this->conn,$query);
	}

	public function closeConnection()
	{
		mysqli_close($this->conn);
	}

	public function selectData(){
		$query = "SELECT * FROM contactlist";
		$result = mysqli_query($this->conn, $query);
		return $result;
	}

	public function updatetData($lastname,$firstname,$email,$contact_no,$no){
		$query = "UPDATE contactlist SET LastName = '$lastname', FirstName = '$firstname', ContactNumber = '$contact_no', Email = '$email' WHERE No = $no";
		$result = mysqli_query($this->conn, $query);
		return $result;
	}

}